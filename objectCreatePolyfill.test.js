const objectCreate = require("./objectCreatePolyfill");

describe("SyncPromise", () => {
    it("базовый кейс", () => {
        const base = {};
        const test = objectCreate(base);
        expect(base.isPrototypeOf(test)).toBe(true);
    });
});
