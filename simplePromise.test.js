const SimplePromise = require("./SimplePromise");

describe("SyncPromise", () => {
  it("базовый кейс", () => {
    const createAsyncIncrementSimplePromise = value =>
        new SimplePromise(resolve => {
          setTimeout(() => resolve(value + 1));
        });

    const p1 = SimplePromise.resolve(1);
    const p2 = SimplePromise.resolve(
        SimplePromise.resolve(SimplePromise.resolve(2))
    );

    const p3 = createAsyncIncrementSimplePromise(1).then(
        createAsyncIncrementSimplePromise
    );
    const p4 = p3.then(createAsyncIncrementSimplePromise);

    return SimplePromise.all([p1, p2, p3, p4, 5]).then(result => {
      expect(result).toEqual([1, 2, 3, 4, 5]);
    });
  });
});
